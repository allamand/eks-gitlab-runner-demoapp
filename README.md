# Demo app for GitLab multi-architecture container image builder example

This repository contains a small webserver written in Go. Its only
functionality is to render an HTML page that displays the serving
host's CPU architecture (e.g., arm64 or amd64) along with a "Hello
world" message. It is intended to be built into a Linux container
image and then deployed into a multi-architecture container image.

See the [GitLab Multi-Arch Runner Demo](https://gitlab.com/aws-samples/graviton-specialists/eks-gitlab-runner) project for additional
information.

# LICENSE

MIT-0
