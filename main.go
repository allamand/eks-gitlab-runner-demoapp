package main

import (
	"fmt"
	"io"
	"net/http"
	"runtime"
	"time"
)

const addr = ":8080"

func main() {
	http.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
		r.Header.Set("Content-Type", "text/plain")
		io.WriteString(w, "Hello world!\n")
		fmt.Fprintf(w, "Host architecture: %s\n", runtime.GOARCH)
		// log request
		fmt.Printf("%s request for %s from %s\n", time.Now().Format(time.RFC3339), r.RequestURI, r.RemoteAddr)
	})

	fmt.Println("Listening on " + addr)
	http.ListenAndServe(addr, nil)
}
