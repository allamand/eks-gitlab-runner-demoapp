FROM public.ecr.aws/docker/library/golang:1.18.1-bullseye AS build

EXPOSE 8080

WORKDIR /build
COPY . ./
RUN go build -o server

FROM public.ecr.aws/debian/debian:bullseye-slim
COPY --from=build /build/server /server

ENTRYPOINT ["/server"]